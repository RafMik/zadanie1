# dane do obliczenia wartości zadłużenia dla każdego miesiąca
pozyczka = 12000
rata = 200
oprocentowanie = 3

inflacja_styczen1 = 1.59282448436825
inflacja_luty1 = -0.453509101198007
inflacja_marzec1 = 2.32467171712441
inflacja_kwiecień1 = 1.26125440724877
inflacja_maj1 = 1.78252628571251
inflacja_czerwiec1 = 2.32938454145522
inflacja_lipiec1 = 1.50222984223283
inflacja_sierpien1 = 1.78252628571251
inflacja_wrzesien1 = 2.32884899407637
inflacja_pazdziernik1 = 0.616921348207244
inflacja_listopad1 = 2.35229588637833
inflacja_grudzien1 = 0.337779545187098
inflacja_styczen2 = 1.57703524727525
inflacja_luty2 = -0.292781442607648
inflacja_marzec2 = 2.48619659017508
inflacja_kwiecien2 = 0.267110317834564
inflacja_maj2 = 1.41795267229799
inflacja_czerwiec2 = 1.05424326726375
inflacja_lipiec2 = 1.4805201044812
inflacja_sierpien2 = 1.57703524727525
inflacja_wrzesien2 = -0.0774206903147018
inflacja_pazdziernik2 = 1.16573339872354
inflacja_listopad2 = -0.404186717638335
inflacja_grudzien2 = 1.49970852083123


# styczeń1
print("styczeń:")
print((1 + ((inflacja_styczen1+oprocentowanie)/1200)) * pozyczka - rata)
kwota_kredytu_po_styczniu1 = (1 + ((inflacja_styczen1+oprocentowanie)/1200)) * pozyczka - rata
roznica_zadluzenia = float(pozyczka - kwota_kredytu_po_styczniu1)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_styczniu1}, to {roznica_zadluzenia} mniej niz w poprzednim miesiacu")

print("luty:")
kwota_kredytu_po_lutym1 = (1 + ((inflacja_luty1+oprocentowanie)/1200)) * kwota_kredytu_po_styczniu1 - rata
print(kwota_kredytu_po_lutym1)
roznica_zadluzenia1 = float(kwota_kredytu_po_styczniu1 - kwota_kredytu_po_lutym1)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_lutym1}, to {roznica_zadluzenia1} mniej niz w poprzednim miesiacu")

#marzec1
print("marzec:")
kwota_kredytu_po_marcu1 = (1 + ((inflacja_marzec1+oprocentowanie)/1200)) * kwota_kredytu_po_lutym1 - rata
print(kwota_kredytu_po_marcu1)
roznica_zadluzenia2 = float(kwota_kredytu_po_lutym1 - kwota_kredytu_po_marcu1)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_marcu1}, to {roznica_zadluzenia2} mniej niz w poprzednim miesiacu")

#kwiecień1
print("kwiecień:")
kwota_kredytu_po_kwietniu1 = (1 + ((inflacja_kwiecień1+oprocentowanie)/1200)) * kwota_kredytu_po_marcu1 - rata
print(kwota_kredytu_po_kwietniu1)
roznica_zadluzenia3 = float(kwota_kredytu_po_marcu1 - kwota_kredytu_po_kwietniu1)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_kwietniu1}, to {roznica_zadluzenia3} mniej niz w poprzednim miesiacu")

#maj1
print("maj:")
kwota_kredytu_po_maju1 = (1 + ((inflacja_maj1+oprocentowanie)/1200)) * kwota_kredytu_po_kwietniu1 - rata
print(kwota_kredytu_po_maju1)
roznica_zadluzenia4 = float(kwota_kredytu_po_kwietniu1 - kwota_kredytu_po_maju1)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_maju1}, to {roznica_zadluzenia4} mniej niz w poprzednim miesiacu")

#czerwiec1
print("czerwiec:")
kwota_kredytu_po_czerwcu1 = (1 + ((inflacja_czerwiec1+oprocentowanie)/1200)) * kwota_kredytu_po_maju1 - rata
print(kwota_kredytu_po_czerwcu1)
roznica_zadluzenia5 = float(kwota_kredytu_po_maju1 - kwota_kredytu_po_czerwcu1)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_czerwcu1}, to {roznica_zadluzenia5} mniej niz w poprzednim miesiacu")

#lipiec1
print("lipiec:")
kwota_kredytu_po_lipcu1 = (1 + ((inflacja_lipiec1+oprocentowanie)/1200)) * kwota_kredytu_po_czerwcu1 - rata
print(kwota_kredytu_po_lipcu1)
roznica_zadluzenia6 = float(kwota_kredytu_po_czerwcu1 - kwota_kredytu_po_lipcu1)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_lipcu1}, to {roznica_zadluzenia6} mniej niz w poprzednim miesiacu")

#sierpień1
print("sierpień:")
kwota_kredytu_po_sierpniu1 = (1 + ((inflacja_sierpien1+oprocentowanie)/1200)) * kwota_kredytu_po_lipcu1 - rata
print(kwota_kredytu_po_sierpniu1)
roznica_zadluzenia7 = float(kwota_kredytu_po_lipcu1 - kwota_kredytu_po_sierpniu1)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_sierpniu1}, to {roznica_zadluzenia7} mniej niz w poprzednim miesiacu")

#wrzesień1
print("wrzesień:")
kwota_kredytu_po_wrześniu1 = (1 + ((inflacja_wrzesien1+oprocentowanie)/1200)) * kwota_kredytu_po_sierpniu1 - rata
print(kwota_kredytu_po_wrześniu1)
roznica_zadluzenia8 = float(kwota_kredytu_po_sierpniu1 - kwota_kredytu_po_wrześniu1)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_wrześniu1}, to {roznica_zadluzenia8} mniej niz w poprzednim miesiacu")

#październik1
print("pazdziernik:")
kwota_kredytu_po_pazdzierniku1 = (1 + ((inflacja_pazdziernik1+oprocentowanie)/1200)) * kwota_kredytu_po_wrześniu1 - rata
print(kwota_kredytu_po_pazdzierniku1)
roznica_zadluzenia9 = float(kwota_kredytu_po_wrześniu1 - kwota_kredytu_po_pazdzierniku1)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_pazdzierniku1}, to {roznica_zadluzenia9} mniej niz w poprzednim miesiacu")

#listopad1
print("listopad:")
kwota_kredytu_po_listopadzie1 = (1 + ((inflacja_listopad1+oprocentowanie)/1200)) * kwota_kredytu_po_pazdzierniku1 - rata
print(kwota_kredytu_po_listopadzie1)
roznica_zadluzenia10 = float(kwota_kredytu_po_pazdzierniku1 - kwota_kredytu_po_listopadzie1)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_listopadzie1}, to {roznica_zadluzenia10} mniej niz w poprzednim miesiacu")

#grudzień1
print("grudzień:")
kwota_kredytu_po_grudniu1 = (1 + ((inflacja_grudzien1+oprocentowanie)/1200)) * kwota_kredytu_po_listopadzie1 - rata
print(kwota_kredytu_po_grudniu1)
roznica_zadluzenia11 = float(kwota_kredytu_po_listopadzie1 - kwota_kredytu_po_grudniu1)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_grudniu1}, to {roznica_zadluzenia11} mniej niz w poprzednim miesiacu")

# styczeń2
print("styczeń2:")
print((1 + ((inflacja_styczen2+oprocentowanie)/1200)) * pozyczka - rata)
kwota_kredytu_po_styczniu2 = (1 + ((inflacja_styczen2+oprocentowanie)/1200)) * pozyczka - rata
roznica_zadluzenia12 = float(kwota_kredytu_po_grudniu1 - kwota_kredytu_po_styczniu2)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_styczniu2}, to {roznica_zadluzenia12} mniej niz w poprzednim miesiacu")

#luty2
print("luty2:")
kwota_kredytu_po_lutym2 = (1 + ((inflacja_luty2+oprocentowanie)/1200)) * kwota_kredytu_po_styczniu2 - rata
print(kwota_kredytu_po_lutym2)
roznica_zadluzenia13 = float(kwota_kredytu_po_styczniu2 - kwota_kredytu_po_lutym2)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_styczniu2}, to {roznica_zadluzenia13} mniej niz w poprzednim miesiacu")

#marzec2
print("marzec2:")
kwota_kredytu_po_marcu2 = (1 + ((inflacja_marzec2+oprocentowanie)/1200)) * kwota_kredytu_po_lutym2 - rata
print(kwota_kredytu_po_marcu2)
roznica_zadluzenia14 = float(kwota_kredytu_po_lutym2 - kwota_kredytu_po_marcu2)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_lutym2}, to {roznica_zadluzenia14} mniej niz w poprzednim miesiacu")

#kwiecień2
print("kwiecień2:")
kwota_kredytu_po_kwietniu2 = (1 + ((inflacja_kwiecien2+oprocentowanie)/1200)) * kwota_kredytu_po_marcu2 - rata
print(kwota_kredytu_po_kwietniu2)
roznica_zadluzenia15 = float(kwota_kredytu_po_marcu2 - kwota_kredytu_po_kwietniu2)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_marcu2}, to {roznica_zadluzenia15} mniej niz w poprzednim miesiacu")

#maj2
print("maj2:")
kwota_kredytu_po_maju2 = (1 + ((inflacja_maj2+oprocentowanie)/1200)) * kwota_kredytu_po_kwietniu2 - rata
print(kwota_kredytu_po_maju2)
roznica_zadluzenia16 = float(kwota_kredytu_po_kwietniu2 - kwota_kredytu_po_maju2)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_kwietniu2}, to {roznica_zadluzenia16} mniej niz w poprzednim miesiacu")

#czerwiec2
print("czerwiec2:")
kwota_kredytu_po_czerwcu2 = (1 + ((inflacja_czerwiec2+oprocentowanie)/1200)) * kwota_kredytu_po_maju2 - rata
print(kwota_kredytu_po_czerwcu2)
roznica_zadluzenia17 = float(kwota_kredytu_po_maju2 - kwota_kredytu_po_czerwcu2)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_maju2}, to {roznica_zadluzenia17} mniej niz w poprzednim miesiacu")

#lipiec2
print("lipiec2:")
kwota_kredytu_po_lipcu2 = (1 + ((inflacja_lipiec2+oprocentowanie)/1200)) * kwota_kredytu_po_czerwcu2 - rata
print(kwota_kredytu_po_lipcu2)
roznica_zadluzenia18 = float(kwota_kredytu_po_czerwcu2 - kwota_kredytu_po_lipcu2)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_czerwcu2}, to {roznica_zadluzenia18} mniej niz w poprzednim miesiacu")

#sierpień2
print("sierpień2:")
kwota_kredytu_po_sierpniu2 = (1 + ((inflacja_sierpien2+oprocentowanie)/1200)) * kwota_kredytu_po_lipcu2 - rata
print(kwota_kredytu_po_sierpniu2)
roznica_zadluzenia19 = float(kwota_kredytu_po_lipcu2 - kwota_kredytu_po_sierpniu2)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_sierpniu2}, to {roznica_zadluzenia19} mniej niz w poprzednim miesiacu")

#wrzesień2
print("wrzesień2:")
kwota_kredytu_po_wrześniu2 = (1 + ((inflacja_wrzesien2+oprocentowanie)/1200)) * kwota_kredytu_po_sierpniu2 - rata
print(kwota_kredytu_po_wrześniu2)
roznica_zadluzenia20 = float(kwota_kredytu_po_sierpniu2 - kwota_kredytu_po_wrześniu2)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_wrześniu2}, to {roznica_zadluzenia20} mniej niz w poprzednim miesiacu")

#październik2
print("pazdziernik2:")
kwota_kredytu_po_pazdzierniku2 = (1 + ((inflacja_pazdziernik2+oprocentowanie)/1200)) * kwota_kredytu_po_wrześniu2 - rata
print(kwota_kredytu_po_pazdzierniku2)
roznica_zadluzenia21 = float(kwota_kredytu_po_wrześniu2 - kwota_kredytu_po_pazdzierniku2)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_pazdzierniku2}, to {roznica_zadluzenia21} mniej niz w poprzednim miesiacu")

#listopad2
print("listopad2:")
kwota_kredytu_po_listopadzie2 = (1 + ((inflacja_listopad2+oprocentowanie)/1200)) * kwota_kredytu_po_pazdzierniku2 - rata
print(kwota_kredytu_po_listopadzie2)
roznica_zadluzenia22 = float(kwota_kredytu_po_pazdzierniku2 - kwota_kredytu_po_listopadzie2)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_listopadzie2}, to {roznica_zadluzenia22} mniej niz w poprzednim miesiacu")

#grudzień2
print("grudzień2:")
kwota_kredytu_po_grudniu2 = (1 + ((inflacja_grudzien2+oprocentowanie)/1200)) * kwota_kredytu_po_listopadzie2 - rata
print(kwota_kredytu_po_grudniu2)
roznica_zadluzenia23 = float(kwota_kredytu_po_listopadzie2 - kwota_kredytu_po_grudniu2)
print(f" Twoja pozostala kwota kredytu to {kwota_kredytu_po_listopadzie2}, to {roznica_zadluzenia23} mniej niz w poprzednim miesiacu")


rata_kredytu = input("Podaj ratę kredytu: ")
oprocentowanie_kredytu = input("Podaj oprocentowanie: " )
wartosc_kredyt = input("Podaj wartość kredytu: ")

print(f"rata: {rata_kredytu}, oprocentowanie: {oprocentowanie_kredytu}, wartosc: {wartosc_kredyt}")
